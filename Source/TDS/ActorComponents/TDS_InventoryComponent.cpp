// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_InventoryComponent.h"
#include <TDS/Game/TDSGameInstance.h>

// Sets default values for this component's properties
UTDS_InventoryComponent::UTDS_InventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDS_InventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	for (int8 i = 0; i < WeaponSlots.Num(); i++) {
		UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
		if (myGI) {
			if (!WeaponSlots[i].NameItem.IsNone()) {
				FWeaponInfo Info;
				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info)) {
					WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
				}
				else {
					/*WeaponSlots.RemoveAt(i);
					i--;*/
				}
			}
		}
	}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0)) {
		if (!WeaponSlots[0].NameItem.IsNone()) {
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}
}


// Called every frame
void UTDS_InventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//TODO: Refactoring is needed
bool UTDS_InventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1) {
		CorrectIndex = 0;
	}
	else if (ChangeToIndex < 0) {
		CorrectIndex = WeaponSlots.Num() - 1;
	}
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex)) {
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone()) {
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
			{
				bIsSuccess = true;
			}
			else {
				UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
				if (myGI) {
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 i = 0;
					while (i < AmmoSlots.Num() && !bIsFind) {
						if (AmmoSlots[i].WeaponType == myInfo.WeaponType && AmmoSlots[i].Count > 0) {
							bIsSuccess = true;
							bIsFind = true;
						}
						i++;
					}
				}
			}

			if (bIsSuccess) {
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}

	if (!bIsSuccess) {
		if (bIsForward) {
			int8 iteration = 0;
			int8 SecondIteration = 0;
			while (iteration < WeaponSlots.Num() && !bIsSuccess) {
				iteration++;
				int8 tmpIndex = ChangeToIndex + iteration;
				if (WeaponSlots.IsValidIndex(tmpIndex)) {
					if (!WeaponSlots[tmpIndex].NameItem.IsNone()) {
						if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0) {
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
							NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
							if (myGI) {
								myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

								bool bIsFind = false;
								int8 i = 0;
								while (i < AmmoSlots.Num() && !bIsFind) {
									if (AmmoSlots[i].WeaponType == myInfo.WeaponType && AmmoSlots[i].Count > 0) {
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
										NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
										NewCurrentIndex = tmpIndex;
										bIsFind = true;
									}
									i++;
								}
							}
						}
					}
				}
				else {
					if (OldIndex != SecondIteration) {
						if (WeaponSlots.IsValidIndex(SecondIteration)) {
							if (!WeaponSlots[SecondIteration].NameItem.IsNone()) {
								if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0) {
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
									NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
									NewCurrentIndex = SecondIteration;
								}
								else
								{
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
									if (myGI) {
										myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

										bool bIsFind = false;
										int8 i = 0;
										while (i < AmmoSlots.Num() && !bIsFind) {
											if (AmmoSlots[i].WeaponType == myInfo.WeaponType && AmmoSlots[i].Count > 0) {
												bIsSuccess = true;
												NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
												NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
												NewCurrentIndex = SecondIteration;
												bIsFind = true;
											}
											i++;
										}
									}
								}
							}
						}
					}
					else {
						if (WeaponSlots.IsValidIndex(SecondIteration)) {
							if (!WeaponSlots[SecondIteration].NameItem.IsNone()) {
								if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0) {

								}
								else {
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
									if (myGI) {
										myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

										bool bIsFind = false;
										int8 i = 0;
										while (i < AmmoSlots.Num() && !bIsFind) {
											if (AmmoSlots[i].WeaponType == myInfo.WeaponType) {
												if (AmmoSlots[i].Count > 0) {

												}
												else {
													UE_LOG(LogTemp, Error, TEXT("UTDS_InventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
												}
											}
											i++;
										}
									}
								}
							}
						}
					}
					SecondIteration++;
				}
			}
		}
		else {
			int8 iteration = 0;
			int8 SecondIteration = WeaponSlots.Num() - 1;
			while (iteration < WeaponSlots.Num() && !bIsSuccess) {
				iteration++;
				int8 tmpIndex = ChangeToIndex - iteration;
				if (WeaponSlots.IsValidIndex(tmpIndex)) {
					if (!WeaponSlots[tmpIndex].NameItem.IsNone()) {
						if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0) {
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
							NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
							if (myGI) {
								myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

								bool bIsFind = false;
								int8 i = 0;
								while (i < AmmoSlots.Num() && !bIsFind) {
									if (AmmoSlots[i].WeaponType == myInfo.WeaponType && AmmoSlots[i].Count > 0) {
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
										NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
										NewCurrentIndex = tmpIndex;
										bIsFind = true;
									}
									i++;
								}
							}
						}
					}
				}
				else {
					if (OldIndex != SecondIteration) {
						if (WeaponSlots.IsValidIndex(SecondIteration)) {
							if (!WeaponSlots[SecondIteration].NameItem.IsNone()) {
								if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0) {
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
									NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
									NewCurrentIndex = SecondIteration;
								}
								else
								{
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
									if (myGI) {
										myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

										bool bIsFind = false;
										int8 i = 0;
										while (i < AmmoSlots.Num() && !bIsFind) {
											if (AmmoSlots[i].WeaponType == myInfo.WeaponType && AmmoSlots[i].Count > 0) {
												bIsSuccess = true;
												NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
												NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
												NewCurrentIndex = SecondIteration;
												bIsFind = true;
											}
											i++;
										}
									}
								}
							}
						}
					}
					else {
						if (WeaponSlots.IsValidIndex(SecondIteration)) {
							if (!WeaponSlots[SecondIteration].NameItem.IsNone()) {
								if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0) {

								}
								else {
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
									if (myGI) {
										myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

										bool bIsFind = false;
										int8 i = 0;
										while (i < AmmoSlots.Num() && !bIsFind) {
											if (AmmoSlots[i].WeaponType == myInfo.WeaponType) {
												if (AmmoSlots[i].Count > 0) {

												}
												else {
													UE_LOG(LogTemp, Error, TEXT("UTDS_InventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
												}
											}
											i++;
										}
									}
								}
							}
						}
					}
					SecondIteration--;
				}
			}
		}
	}
	
	if (bIsSuccess) {
		SetAdditionalInitWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UTDS_InventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind) {
			if (i == IndexWeapon) {
				result = WeaponSlots[9].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind) {
			UE_LOG(LogTemp, Warning, TEXT("UTDS_InventoryComponent::GetAdditionalWeapon - No Found Weapon with IndexWeapon - $d"), IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDS_InventoryComponent::GetAdditionalWeapon - Not Correct IndexWeapon - $d"), IndexWeapon);
	}
	return result;
}

int32 UTDS_InventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind) {
		if (WeaponSlots[i].NameItem == IdWeaponName) {
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

FName UTDS_InventoryComponent::GetWeaponNameSlotByIndex(int32 indexSlot)
{
	FName result;
	if (WeaponSlots.IsValidIndex(indexSlot)) {
		result = WeaponSlots[indexSlot].NameItem;
	}
	return result;
}

void UTDS_InventoryComponent::SetAdditionalInitWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind) {
			if (i == IndexWeapon) {
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind) {
			UE_LOG(LogTemp, Warning, TEXT("UTDS_InventoryComponent::SetAdditionalWeapon - No Found Weapon with IndexWeapon - $d"), IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDS_InventoryComponent::SetAdditionalWeapon - Not Correct IndexWeapon - $d"), IndexWeapon);
	}
}

void UTDS_InventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CountChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind) {
		if (AmmoSlots[i].WeaponType == TypeWeapon) {
			AmmoSlots[i].Count += CountChangeAmmo;
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount) {
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
			}
			OnAmmoChange.Broadcast(TypeWeapon, AmmoSlots[i].Count);
			bIsFind = true;
		}
		i++;
	}
}

bool UTDS_InventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon) {
			bIsFind = true;
			AvailableAmmoForWeapon = AmmoSlots[i].Count;
			if (AmmoSlots[i].Count>0) {
				return true;
			}
		}
		i++;
	}
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);

	return false;
}

bool UTDS_InventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result) {
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Count < AmmoSlots[i].MaxCount) {
			result = true;
		}
		i++;
	}
	return result;
}

bool UTDS_InventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone() && !bIsFreeSlot) {
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UTDS_InventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
	bool result = false;
	//FDropItem DropItemInfo;
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo)) {
		WeaponSlots[IndexSlot] = NewWeapon;
		SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);
		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		
		result = true;
	}
	return result;
}

bool UTDS_InventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 indexSlot = -1;
	if (CheckCanTakeWeapon(indexSlot)) {
		if (WeaponSlots.IsValidIndex(indexSlot)) {
			WeaponSlots[indexSlot] = NewWeapon;

			OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
			return true;
		}
	}
	//OnSwitchWeapon.Broadcast()
	return false;
}

bool UTDS_InventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool result = false;

	FName DropItemName = GetWeaponNameSlotByIndex(IndexSlot);
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI) {
		result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot)) {
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}
	return result;
}
