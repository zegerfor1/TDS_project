// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "../TDS.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass) {
		UTDS_StateEffect* myEffect = Cast<UTDS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect) {
			bool bIsCanAdd = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd) {
				if (myEffect->PossibleInteractSurface[i] == SurfaceType) {
					bIsCanAdd = true;
					UTDS_StateEffect* NewEffect = NewObject< UTDS_StateEffect>(TakeEffectActor, FName("Effect"));
					if (NewEffect) {
						NewEffect->InitObject();
					}
				}
				i++;
			}
		}
	}
}
